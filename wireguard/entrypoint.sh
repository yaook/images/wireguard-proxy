#!/bin/bash

##
## Copyright (c) 2022 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

set -euo pipefail
umask 0077
wg_iface="${WG_IFACE:-wg0}"
wg_conf="/etc/wireguard/$wg_iface.conf"
cat > "$wg_conf" <<EOF
[Interface]
PrivateKey = $WG_PRIVATE_KEY
Address = $WG_LOCAL_IP

[Peer]
PublicKey = $WG_PEER_PUBLIC_KEY
Endpoint = $WG_PEER_ENDPOINT
PersistentKeepalive = 25
EOF

if [ -n "${WG_PEER_ALLOWED_IPS:-}" ]; then
  printf 'AllowedIPs = %s' "$WG_PEER_ALLOWED_IPS" >> "$wg_conf"
fi

wg-quick up "$wg_iface"
exec tail -f /dev/null
